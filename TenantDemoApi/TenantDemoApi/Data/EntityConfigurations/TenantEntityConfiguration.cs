﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Reflection.Emit;
using TenantDemoApplication.Models.Domain;

namespace TenantDemoApplication.Data.EntityConfigurations
{
    public class TenantEntityConfiguration : IEntityTypeConfiguration<Tenant>
    {
        public void Configure(EntityTypeBuilder<Tenant> builder)
        {
            builder
            .HasKey(t => t.Id);

            builder
                .Property(t => t.Name)
            .HasMaxLength(500);

            builder
                .Property(t => t.Address)
                .HasMaxLength(500);

            builder
                .Property(t => t.Explanation)
                .HasMaxLength(500);

            builder
                .Property(t => t.PassiveReason)
                .HasMaxLength(500);
        }
    }
}
