﻿using Microsoft.AspNetCore.Http.HttpResults;
using TenantDemoApplication.Models.Domain;
using TenantDemoApplication.Services;
using TenantDemoApplication.Models.Dto.Tenant;
using Microsoft.AspNetCore.Mvc;
using TenantDemoApi.Helpers;
using DevExtreme.AspNet.Data;
using TenantDemoApplication.Data;
using FastReport.Export.PdfSimple;
using FastReport;

namespace TenantDemoApplication.Endpoints
{
    public static class TenantEndpoints
    {
        public static void MapTenantEndpoints(this IEndpointRouteBuilder routes)
        {
            var group = routes.MapGroup("/api/Tenant").WithTags(nameof(Tenant));

            group.MapGet("/", (DataSourceLoadOptions sourceOptions, ApplicationDbContext context) =>
            {
                return DataSourceLoader.LoadAsync(context.Tenants, sourceOptions);
            })
            .WithName("GetAllTenants")
            .WithOpenApi();

            group.MapGet("/{id}", async Task<Results<Ok<TenantDto>, NotFound>> (int id, [FromServices] ITenantService tenantService) =>
            {
                return await tenantService.GetTenantAsync(id)
                    is TenantDto model
                        ? TypedResults.Ok(model)
                        : TypedResults.NotFound();
            })
            .WithName("GetTenantById")
            .WithOpenApi();

            group.MapPut("/{id}", async Task<Results<Ok, NotFound>> (int id, [FromBody] BaseTenantDto tenant, [FromServices] ITenantService tenantService) =>
            {
                return await tenantService.UpdateTenantAsync(id, tenant) ? TypedResults.Ok() : TypedResults.NotFound();
            })
            .WithName("UpdateTenant")
            .WithOpenApi();

            group.MapPost("/", async ([FromBody] BaseTenantDto tenant, [FromServices] ITenantService tenantService) =>
            {
                var result = await tenantService.AddTenantAsync(tenant);
                return TypedResults.Created($"/api/Tenant/{result}", tenant);
            })
            .WithName("CreateTenant")
            .WithOpenApi();

            group.MapPost("/Print", async ([FromServices] ITenantService tenantService) =>
            {
                var path = Path.Combine(Environment.CurrentDirectory, "Reports", "Tenants.frx");
                Report report = new();
                report.Report.Load(path);
                report.Report.RegisterData(await tenantService.GetAllTenantsAsync(), "Tenants");
                report.Prepare();
                PDFSimpleExport pdf = new();
                var memoryStream = new MemoryStream();
                pdf.Export(report, memoryStream);
                memoryStream.Position = 0;
                return TypedResults.File(memoryStream.ToArray(), fileDownloadName: "Tenants.pdf");
            })
                .Produces<FileResult>(contentType: "application/pdf")
            .WithName("PrintTenants")
            .WithOpenApi();

            group.MapDelete("/", async Task<Results<Ok, NotFound>> (HttpContext httpContext, [FromServices] ITenantService tenantService) =>
            {
                if (int.TryParse(httpContext.Request.Form["key"], out int id))
                {
                    return await tenantService.DeleteTenantAsync(id) ? TypedResults.Ok() : TypedResults.NotFound();

                }
                return TypedResults.NotFound();
            })
            .WithName("DeleteTenant")
            .WithOpenApi();
        }
    }
}
