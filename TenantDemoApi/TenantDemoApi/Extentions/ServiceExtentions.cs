﻿using Microsoft.EntityFrameworkCore;
using TenantDemoApplication.Data;
using TenantDemoApplication.Services;

namespace TenantDemoApplication.Extentions
{
    public static class ServiceExtentions
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<ITenantService, TenantService>();
        }
        public static async Task MigrateDatabaseAsync(this IApplicationBuilder builder)
        {
            using var scope = builder.ApplicationServices.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            await context.Database.MigrateAsync();
        }
    }
}
