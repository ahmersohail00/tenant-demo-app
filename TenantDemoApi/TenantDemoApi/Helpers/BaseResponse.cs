﻿using X.PagedList;

namespace TenantDemoApi.Helpers
{
    public class BaseResponse<T>
    {
        public T Data { get; set; }
    }

    public class PageResponse<T>
    {
        public IPagedList<T> Data { get; private set; }
        public PagedListMetaData Pagination { get; private set; }

        public PageResponse(IPagedList<T> data)
        {
            Data = data;
            Pagination = data.GetMetaData();
        }

    }
}
