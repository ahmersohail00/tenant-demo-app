﻿using AutoMapper;
using TenantDemoApplication.Models.Domain;
using TenantDemoApplication.Models.Dto.Tenant;

namespace TenantDemoApplication.MappingProfiles
{
    public class TenantProfile : Profile
    {
        public TenantProfile()
        {
            CreateMap<Tenant, TenantDto>().ReverseMap();
            CreateMap<Tenant, BaseTenantDto>().ReverseMap();
        }
    }
}
