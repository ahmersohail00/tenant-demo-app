﻿namespace TenantDemoApplication.Models.Domain
{
    public class Tenant
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public required string Code { get; set; }
        public short Type { get; set; }
        public required string Name { get; set; }
        public required string Address { get; set; }
        public required string Explanation { get; set; }
        public short DataBaseType { get; set; }
        public bool IsSharedDataBase { get; set; }
        public required string ConnectionString { get; set; }
        public bool IsActive { get; set; }
        public required string PassiveReason { get; set; }
        public bool IsOnlyTenant { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public int? DeletedBy { get; set; }
    }
}
