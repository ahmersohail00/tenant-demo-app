﻿namespace TenantDemoApplication.Models.Dto.Tenant
{
    public class BaseTenantDto
    {
        public required string Code { get; set; }
        public short Type { get; set; }
        public required string Name { get; set; }
        public required string Address { get; set; }
        public required string Explanation { get; set; }
        public short DataBaseType { get; set; }
        public bool IsSharedDataBase { get; set; }
        public required string ConnectionString { get; set; }
        public bool IsActive { get; set; }
        public required string PassiveReason { get; set; }
        public bool IsOnlyTenant { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;
    }
}
