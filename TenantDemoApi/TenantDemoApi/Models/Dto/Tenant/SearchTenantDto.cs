﻿using Azure.Core;

namespace TenantDemoApplication.Models.Dto.Tenant
{
    public class SearchTenantDto
    {
        public int PageNo { get; private set; } = 1;
        public int PageSize { get; private set; } = 10;
        public string? Code { get; set; }
        public short? Type { get; set; }
        public string? Name { get; set; }
        public string? Address { get; set; }
        public bool? IsSharedDataBase { get; set; }
    }
}
