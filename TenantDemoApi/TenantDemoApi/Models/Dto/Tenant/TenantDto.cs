﻿namespace TenantDemoApplication.Models.Dto.Tenant
{
    public class TenantDto: BaseTenantDto
    {
        public int Id { get; set; }
    }
}
