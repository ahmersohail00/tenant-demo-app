﻿using TenantDemoApplication.Models.Dto.Tenant;
using X.PagedList;

namespace TenantDemoApplication.Services
{
    public interface ITenantService
    {
        Task<IPagedList<TenantDto>> GetTenantsAsync(SearchTenantDto searchDto);
        Task<List<TenantDto>> GetAllTenantsAsync();
        Task<TenantDto> GetTenantAsync(int id);
        Task<bool> UpdateTenantAsync(int id, BaseTenantDto tenantDto);
        Task<int> AddTenantAsync(BaseTenantDto tenantDto);
        Task<bool> DeleteTenantAsync(int id);
    }
}
