﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TenantDemoApplication.Data;
using TenantDemoApplication.Models.Domain;
using TenantDemoApplication.Models.Dto.Tenant;
using X.PagedList;

namespace TenantDemoApplication.Services
{
    public class TenantService : ITenantService
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public TenantService(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IPagedList<TenantDto>> GetTenantsAsync(SearchTenantDto searchDto)
        {
            var tenantQuery = _context.Tenants
                .Where(t =>
                (string.IsNullOrWhiteSpace(searchDto.Code) || t.Code.Contains(searchDto.Code))
                && (string.IsNullOrWhiteSpace(searchDto.Name) || t.Name.Contains(searchDto.Name))
                && (!searchDto.Type.HasValue || t.Type == searchDto.Type)
                && (string.IsNullOrWhiteSpace(searchDto.Address) || t.Address.Contains(searchDto.Address))
                && (!searchDto.IsSharedDataBase.HasValue || t.IsSharedDataBase == searchDto.IsSharedDataBase));

            var tenantCount = await tenantQuery.CountAsync();

            var tenants = await tenantQuery
                .Skip((searchDto.PageNo - 1) * searchDto.PageSize)
                .Take(searchDto.PageSize)
                .ToListAsync();

            var mappedTenants = _mapper.Map<List<TenantDto>>(tenants);

            var paginatedTenants = new StaticPagedList<TenantDto>(mappedTenants, searchDto.PageNo, searchDto.PageSize, tenantCount);

            return paginatedTenants;
        }

        public async Task<TenantDto> GetTenantAsync(int id)
        {
            var tenant = await _context.Tenants
                .AsNoTracking()
                .FirstOrDefaultAsync(t => t.Id == id);
            return _mapper.Map<TenantDto>(tenant);
        }

        public async Task<bool> UpdateTenantAsync(int id, BaseTenantDto tenantDto)
        {
            var affected = await _context.Tenants
                    .Where(model => model.Id == id)
                    .ExecuteUpdateAsync(setters => setters
                      .SetProperty(m => m.Code, tenantDto.Code)
                      .SetProperty(m => m.Type, tenantDto.Type)
                      .SetProperty(m => m.Name, tenantDto.Name)
                      .SetProperty(m => m.Address, tenantDto.Address)
                      .SetProperty(m => m.Explanation, tenantDto.Explanation)
                      .SetProperty(m => m.DataBaseType, tenantDto.DataBaseType)
                      .SetProperty(m => m.IsSharedDataBase, tenantDto.IsSharedDataBase)
                      .SetProperty(m => m.ConnectionString, tenantDto.ConnectionString)
                      .SetProperty(m => m.IsActive, tenantDto.IsActive)
                      .SetProperty(m => m.PassiveReason, tenantDto.PassiveReason)
                      .SetProperty(m => m.IsOnlyTenant, tenantDto.IsOnlyTenant)
                      .SetProperty(m => m.ModifiedBy, 1)
                      .SetProperty(m => m.ModifiedOn, DateTime.Now)
                    );
            return affected == 1;
        }

        public async Task<int> AddTenantAsync(BaseTenantDto tenantDto)
        {
            var tenant = _mapper.Map<Tenant>(tenantDto);
            _context.Tenants.Add(tenant);
            await _context.SaveChangesAsync();
            return tenant.Id;
        }

        public async Task<bool> DeleteTenantAsync(int id)
        {
            var affected = await _context.Tenants
                    .Where(model => model.Id == id)
                    .ExecuteDeleteAsync();

            return affected == 1;
        }

        public async Task<List<TenantDto>> GetAllTenantsAsync()
        {
            var tenants = await _context.Tenants.ToListAsync();
            return _mapper.Map<List<TenantDto>>(tenants);

        }
    }
}
