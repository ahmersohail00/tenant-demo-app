/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StrictHttpResponse } from '../../strict-http-response';
import { RequestBuilder } from '../../request-builder';

import { BaseTenantDto } from '../../models/base-tenant-dto';

export interface CreateTenant$Params {
      body: BaseTenantDto
}

export function createTenant(http: HttpClient, rootUrl: string, params: CreateTenant$Params, context?: HttpContext): Observable<StrictHttpResponse<BaseTenantDto>> {
  const rb = new RequestBuilder(rootUrl, createTenant.PATH, 'post');
  if (params) {
    rb.body(params.body, 'application/json');
  }

  return http.request(
    rb.build({ responseType: 'json', accept: 'application/json', context })
  ).pipe(
    filter((r: any): r is HttpResponse<any> => r instanceof HttpResponse),
    map((r: HttpResponse<any>) => {
      return r as StrictHttpResponse<BaseTenantDto>;
    })
  );
}

createTenant.PATH = '/api/Tenant';
