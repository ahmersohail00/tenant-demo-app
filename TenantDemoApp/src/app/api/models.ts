/* tslint:disable */
/* eslint-disable */
export { BaseTenantDto } from './models/base-tenant-dto';
export { LoadResult } from './models/load-result';
export { TenantDto } from './models/tenant-dto';
