/* tslint:disable */
/* eslint-disable */
export interface BaseTenantDto {
  address?: null | string;
  code?: null | string;
  connectionString?: null | string;
  creationDate?: string;
  dataBaseType?: number;
  explanation?: null | string;
  isActive?: boolean;
  isOnlyTenant?: boolean;
  isSharedDataBase?: boolean;
  name?: null | string;
  passiveReason?: null | string;
  type?: number;
}
