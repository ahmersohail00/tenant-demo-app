/* tslint:disable */
/* eslint-disable */
export interface TenantDto {
  address?: null | string;
  code?: null | string;
  connectionString?: null | string;
  creationDate?: string;
  dataBaseType?: number;
  explanation?: null | string;
  id?: number;
  isActive?: boolean;
  isOnlyTenant?: boolean;
  isSharedDataBase?: boolean;
  name?: null | string;
  passiveReason?: null | string;
  type?: number;
}
