/* tslint:disable */
/* eslint-disable */
import { HttpClient, HttpContext } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';

import { BaseTenantDto } from '../models/base-tenant-dto';
import { createTenant } from '../fn/tenant/create-tenant';
import { CreateTenant$Params } from '../fn/tenant/create-tenant';
import { deleteTenant } from '../fn/tenant/delete-tenant';
import { DeleteTenant$Params } from '../fn/tenant/delete-tenant';
import { getAllTenants } from '../fn/tenant/get-all-tenants';
import { GetAllTenants$Params } from '../fn/tenant/get-all-tenants';
import { getTenantById } from '../fn/tenant/get-tenant-by-id';
import { GetTenantById$Params } from '../fn/tenant/get-tenant-by-id';
import { LoadResult } from '../models/load-result';
import { printTenants } from '../fn/tenant/print-tenants';
import { PrintTenants$Params } from '../fn/tenant/print-tenants';
import { TenantDto } from '../models/tenant-dto';
import { updateTenant } from '../fn/tenant/update-tenant';
import { UpdateTenant$Params } from '../fn/tenant/update-tenant';

@Injectable({ providedIn: 'root' })
export class TenantService extends BaseService {
  constructor(config: ApiConfiguration, http: HttpClient) {
    super(config, http);
  }

  /** Path part for operation `getAllTenants()` */
  static readonly GetAllTenantsPath = '/api/Tenant';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllTenants()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllTenants$Response(params?: GetAllTenants$Params, context?: HttpContext): Observable<StrictHttpResponse<LoadResult>> {
    return getAllTenants(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getAllTenants$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllTenants(params?: GetAllTenants$Params, context?: HttpContext): Observable<LoadResult> {
    return this.getAllTenants$Response(params, context).pipe(
      map((r: StrictHttpResponse<LoadResult>): LoadResult => r.body)
    );
  }

  /** Path part for operation `createTenant()` */
  static readonly CreateTenantPath = '/api/Tenant';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createTenant()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createTenant$Response(params: CreateTenant$Params, context?: HttpContext): Observable<StrictHttpResponse<BaseTenantDto>> {
    return createTenant(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `createTenant$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createTenant(params: CreateTenant$Params, context?: HttpContext): Observable<BaseTenantDto> {
    return this.createTenant$Response(params, context).pipe(
      map((r: StrictHttpResponse<BaseTenantDto>): BaseTenantDto => r.body)
    );
  }

  /** Path part for operation `deleteTenant()` */
  static readonly DeleteTenantPath = '/api/Tenant';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteTenant()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteTenant$Response(params?: DeleteTenant$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return deleteTenant(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `deleteTenant$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteTenant(params?: DeleteTenant$Params, context?: HttpContext): Observable<void> {
    return this.deleteTenant$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `getTenantById()` */
  static readonly GetTenantByIdPath = '/api/Tenant/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getTenantById()` instead.
   *
   * This method doesn't expect any request body.
   */
  getTenantById$Response(params: GetTenantById$Params, context?: HttpContext): Observable<StrictHttpResponse<TenantDto>> {
    return getTenantById(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `getTenantById$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getTenantById(params: GetTenantById$Params, context?: HttpContext): Observable<TenantDto> {
    return this.getTenantById$Response(params, context).pipe(
      map((r: StrictHttpResponse<TenantDto>): TenantDto => r.body)
    );
  }

  /** Path part for operation `updateTenant()` */
  static readonly UpdateTenantPath = '/api/Tenant/{id}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateTenant()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateTenant$Response(params: UpdateTenant$Params, context?: HttpContext): Observable<StrictHttpResponse<void>> {
    return updateTenant(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `updateTenant$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateTenant(params: UpdateTenant$Params, context?: HttpContext): Observable<void> {
    return this.updateTenant$Response(params, context).pipe(
      map((r: StrictHttpResponse<void>): void => r.body)
    );
  }

  /** Path part for operation `printTenants()` */
  static readonly PrintTenantsPath = '/api/Tenant/Print';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `printTenants()` instead.
   *
   * This method doesn't expect any request body.
   */
  printTenants$Response(params?: PrintTenants$Params, context?: HttpContext): Observable<StrictHttpResponse<Blob>> {
    return printTenants(this.http, this.rootUrl, params, context);
  }

  /**
   * This method provides access only to the response body.
   * To access the full response (for headers, for example), `printTenants$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  printTenants(params?: PrintTenants$Params, context?: HttpContext): Observable<Blob> {
    return this.printTenants$Response(params, context).pipe(
      map((r: StrictHttpResponse<Blob>): Blob => r.body)
    );
  }

}
