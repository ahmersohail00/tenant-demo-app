export const navigation = [
  {
    text: 'Home',
    path: '/home',
    icon: 'home',
  },
  {
    text: 'Tenant',
    path: '/tenant',
    icon: 'group',
  },
];
