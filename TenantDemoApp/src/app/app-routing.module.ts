import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  LoginFormComponent,
  ResetPasswordFormComponent,
  CreateAccountFormComponent,
  ChangePasswordFormComponent,
} from './shared/components';
import { AuthGuardService } from './shared/services';
import { HomeComponent } from './pages/home/home.component';
import {
  DxButtonModule,
  DxDataGridModule,
  DxFormModule,
  DxLoadIndicatorModule,
  DxSpeedDialActionModule,
} from 'devextreme-angular';
import { TenantComponent } from './pages/tenant/tenant.component';
import { AddTenantComponent } from './pages/tenant/add-tenant/add-tenant.component';
import { SingleCardModule } from './layouts/single-card/single-card.component';
import { TenantService } from './api/services';
import { CommonModule } from '@angular/common';
import { EditTenantComponent } from './pages/tenant/edit-tenant/edit-tenant.component';

const routes: Routes = [
  {
    path: 'tenant',
    component: TenantComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'tenant/add',
    component: AddTenantComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'tenant/edit/:id',
    component: EditTenantComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'login-form',
    component: LoginFormComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'reset-password',
    component: ResetPasswordFormComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'create-account',
    component: CreateAccountFormComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'change-password/:recoveryCode',
    component: ChangePasswordFormComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: '**',
    redirectTo: 'home',
  },
];

@NgModule({
  providers: [AuthGuardService, TenantService],
  exports: [RouterModule],
  declarations: [
    HomeComponent,
    TenantComponent,
    AddTenantComponent,
    EditTenantComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { useHash: true }),
    DxDataGridModule,
    DxFormModule,
    SingleCardModule,
    DxButtonModule,
    DxSpeedDialActionModule,
    DxLoadIndicatorModule,
  ],
})
export class AppRoutingModule {}
