import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs';
import notify from 'devextreme/ui/notify';
import { TenantService } from 'src/app/api/services';
import { BaseTenantDto } from 'src/app/api/models';

@Component({
  selector: 'app-add-tenant',
  templateUrl: './add-tenant.component.html',
  styleUrls: ['./add-tenant.component.scss'],
})
export class AddTenantComponent implements OnInit {
  loading = false;
  formData: BaseTenantDto = {};
  constructor(private tenantService: TenantService, private router: Router) {}

  ngOnInit(): void {
    this.formData = {};
  }

  onSubmit(e: Event) {
    e.preventDefault();
    this.loading = true;

    this.tenantService
      .createTenant({body:this.formData})
      .pipe(
        finalize(() => {
          this.loading = false;
        })
      )
      .subscribe((res) => {
        this.router.navigate(['/tenant']);
      },(error)=>{
        notify(error.error,'Error');
      });
  }
}
