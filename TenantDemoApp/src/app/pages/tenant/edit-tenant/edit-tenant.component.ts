import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { finalize } from 'rxjs';
import { BaseTenantDto } from 'src/app/api/models';
import { TenantService } from 'src/app/api/services';

@Component({
  selector: 'app-edit-tenant',
  templateUrl: './edit-tenant.component.html',
  styleUrls: ['./edit-tenant.component.scss'],
})
export class EditTenantComponent implements OnInit {
  loading = false;
  formData: BaseTenantDto = {};
  id: number;
  constructor(
    private tenantService: TenantService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.tenantService
      .getTenantById({ id: this.id })
      .subscribe((res) => (this.formData = res));
  }

  onSubmit(e: Event) {
    e.preventDefault();
    this.loading = true;

    this.tenantService
      .updateTenant({ id: this.id, body: this.formData })
      .pipe(
        finalize(() => {
          this.loading = false;
        })
      )
      .subscribe(
        (res) => {
          this.router.navigate(['/tenant']);
        },
        (error) => {
          notify(error.error, 'Error');
        }
      );
  }
}
