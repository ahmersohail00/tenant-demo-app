import { Component, OnInit } from '@angular/core';
import { TenantService } from 'src/app/api/services';

import * as AspNetData from 'devextreme-aspnet-data-nojquery';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { saveAs } from 'file-saver';
@Component({
  selector: 'app-tenant',
  templateUrl: './tenant.component.html',
  styleUrls: ['./tenant.component.scss'],
})
export class TenantComponent implements OnInit {
  dataSource: AspNetData.CustomStore;
  pageSize: number = 10;
  pageNo: number = 1;
  url = environment.rootUrl + '/api/Tenant';
  constructor(private tenantService: TenantService, private router: Router) {
    this.editTenant = this.editTenant.bind(this);
  }

  ngOnInit(): void {
    this.dataSource = AspNetData.createStore({
      key: 'id',
      loadUrl: `${this.url}`,
      deleteUrl: this.url,
    });
  }

  addTenant() {
    this.router.navigate(['/tenant', 'add']);
  }

  editTenant(e: any) {
    const id = e.row.data.id;
    this.router.navigate(['/tenant', 'edit', id]);
  }

  print() {
    this.tenantService.printTenants().subscribe((res) => {
      saveAs(res, 'Tenants.pdf');
    });
  }
}
