import { Injectable } from '@angular/core';

@Injectable()
export class AppInfoService {
  constructor() {}

  public get title() {
    return 'TenantDemoApp';
  }

  public get currentYear() {
    return new Date().getFullYear();
  }
}
